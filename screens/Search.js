import {
  View,
  Text,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
} from "react-native";
import React, { useCallback, useState } from "react";
import { AntDesign } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import Loading from "../components/Loading";
var { width, height } = Dimensions.get("window");
import { debounce } from "lodash";
import {
  Images185,
  fallbackMoviePoster,
  fetchingSearchMovies,
} from "../api/MoviesDb";
const Search = () => {
  let navigation = useNavigation();
  let movieName = "spider man And BatMan And Hulk";
  let [results, setResults] = useState([]);
  let [loading, setLoading] = useState(false);
  let [input, setInput] = useState("");

  let handelChnage = (value) => {
    if (value && value.length > 2) {
      setLoading(true);
      fetchingSearchMovies({
        query: value,
        include_adult: "false",
        language: "en-us",
        page: "1",
      }).then((data) => {
        setLoading(false);
        if (data && data.results) setResults(data.results);
      });
    } else {
      setLoading(false);
      setResults([]);
    }
    setInput(value);
  };
  let handleTextDebounce = useCallback(debounce(handelChnage, 400), []);
  return (
    <SafeAreaView className="bg-neutral-800 flex-1 ">
      <View className="mx-4 mb-3 mt-10 flex-row justify-between items-center border border-neutral-400 rounded-full">
        <TextInput
          onChangeText={handleTextDebounce}
          value={input}
          placeholder="Search Movie"
          placeholderTextColor={"lightgray"}
          className="pb-1 pl-6 flex-1 text-base font-semibold text-white  tracking-wider"
        />
        <TouchableOpacity
          className="rounded-full p-3 m-1 bg-neutral-500"
          onPress={() => {
            navigation.navigate("Home");
          }}
        >
          <AntDesign name="close" size={25} color="white" />
        </TouchableOpacity>
      </View>

      {/*list movies */}

      {loading ? (
        <Loading />
      ) : (
        <ScrollView
          className="space-y-3"
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{ paddingHorizontal: 15 }}
        >
          <Text className="text-white font-bold text-lg my-3">
            Results ({results.length})
          </Text>
          <View className="flex-wrap flex-row justify-between">
            {results.length > 0 ? (
              results.map((e, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    onPress={() => navigation.navigate("Movie", e)}
                  >
                    <View className="space-y-2 mb-4">
                      <Image
                        className="rounded-3xl"
                        // source={require("../assets/images/moviePoster2.png")}
                        source={{
                          uri: Images185(e.poster_path) || fallbackMoviePoster,
                        }}
                        style={{ width: width * 0.44, height: height * 0.3 }}
                      />
                      <Text className="text-neutral-400 ml-1">
                        {e.title && e.title.length > 15
                          ? e.title.slice(0, 15) + "..."
                          : e.title}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })
            ) : (
              <View className="flex-row justify-center">
                <Image
                  className="h-96 w-96"
                  source={require("../assets/images/movieTime.png")}
                />
              </View>
            )}
          </View>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default Search;
