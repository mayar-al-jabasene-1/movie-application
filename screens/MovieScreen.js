import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
  Platform,
  StyleSheet,
  Image,
} from "react-native";
import React, { useEffect, useState } from "react";
import { useNavigation, useRoute } from "@react-navigation/native";
import { AntDesign } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import Cost from "../components/Cost";
import MoviesList from "../components/MoviesList";
import Loading from "../components/Loading";
import {
  Images600,
  fallbackMoviePoster,
  fetchingCreditDetails,
  fetchingMoviesDetails,
  fetchingSimilerMovies,
} from "../api/MoviesDb";

var { width, height } = Dimensions.get("window");
const ios = Platform.OS == "ios";
const TopMargin = ios ? "" : "mt-4";
const MovieScreen = () => {
  let { params: item } = useRoute();
  let navegation = useNavigation();
  let [toggle, setToggle] = useState(false);
  let [cost, setCost] = useState([1, 2, 3, 4, 5, 6]);
  let [similer, setSimiler] = useState([1, 2, 3, 4, 5, 6]);
  let [loading, setLoading] = useState(false);
  let [movie, setMovie] = useState({});

  useEffect(() => {
    setLoading(true);
    getMoviesDetails(item.id);
    getMoviesCredits(item.id);
    getSimilarMovies(item.id);
  }, [item]);

  let getMoviesDetails = async (id) => {
    let data = await fetchingMoviesDetails(id);

    if (data) setMovie(data);
    setLoading(false);
  };

  let getMoviesCredits = async (id) => {
    let data = await fetchingCreditDetails(id);
    if (data) setCost(data.cast);
  };
  let getSimilarMovies = async (id) => {
    let data = await fetchingSimilerMovies(id);
    if (data && data.results) setSimiler(data.results);
  };

  let movieName = "spider man And BatMan And Hulk";
  let mm = 90;
  return (
    <ScrollView
      contentContainerStyle={{ paddingBottom: 20 }}
      className="flex-1 bg-neutral-900"
    >
      {/*back button and movie poster */}

      <View className="w-full mt-5">
        <SafeAreaView
          className={
            "z-20 absolute  w-full flex-row justify-between item-center px-4 " +
            TopMargin
          }
        >
          <TouchableOpacity
            onPress={() => navegation.goBack()}
            className="rounded-xl p-1"
            style={style.bggg}
          >
            <AntDesign name="arrowleft" size={28} color="white" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setToggle(!toggle)}>
            <AntDesign
              name="heart"
              size={28}
              color={toggle ? "red" : "white"}
            />
          </TouchableOpacity>
        </SafeAreaView>
        {/*image section */}
        {loading ? (
          <Loading />
        ) : (
          <View>
            <Image
              // source={require("../assets/images/moviePoster2.png")}
              source={{
                uri: Images600(movie.poster_path) || fallbackMoviePoster,
              }}
              style={{ width, height: height * 0.55 }}
              // className="rounded-3xl"
            />
            <LinearGradient
              start={{ x: 0.5, y: 0 }}
              end={{ x: 0.5, y: 1 }}
              colors={["transparent", "rgba(23,23,23,0.8)", "rgba(23,23,23,1)"]}
              style={{ width: width, height: height * 0.4 }}
              className="absolute bottom-0"
            ></LinearGradient>
          </View>
        )}
      </View>
      {/*move details */}
      <View style={{ marginTop: -(height * 0.09) }} className="space-y-3">
        <Text className="text-white text-center text-3xl  font-bold tracking-wide px-4">
          {movie && movie.title}
        </Text>
        {/*status realtime runtime */}
        {movie.id ? (
          <Text className="text-neutral-400 font-semibold text-base text-center">
            {movie && movie.status} - {movie && movie.release_date} -{" "}
            {movie && movie.runtime} min
          </Text>
        ) : null}

        {/*genres */}
        <View className="flex-row  justify-center  mx-4 space-x-2">
          {movie.genres &&
            movie.genres.map((e, index) => {
              let showdots = index + 1 != movie.genres.lenght;
              return (
                <Text
                  key={index}
                  className="text-neutral-400 font-semibold text-base text-center"
                >
                  {e.name} {showdots ? "." : null}
                </Text>
              );
            })}
        </View>
        {/* text for detais*/}
        <Text className="text-neutral-400 mx-4 tracking-wide">
          {movie && movie.overview}
        </Text>
      </View>

      {/*start cost */}
      <Cost cost={cost} navegation={navegation} />

      {/*start movelist */}
      <MoviesList title="Similer Movies" data={similer} hidden={true} />
    </ScrollView>
  );
};
let style = StyleSheet.create({
  icon: {
    marginTop: 5,
  },
  text: {
    color: "#eab308",
  },
  bggg: {
    backgroundColor: "#eab308",
  },
});

export default MovieScreen;
