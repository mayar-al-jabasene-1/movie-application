import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Ionicons } from "@expo/vector-icons";
import { Octicons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import TreadingMovies from "../components/TreadingMovies";
import MoviesList from "../components/MoviesList";
import { useNavigation } from "@react-navigation/native";
import Loading from "../components/Loading";
import {
  fetchingTopTatingMovies,
  fetchingTrandingMovies,
  fetchingUpcomingMovies,
} from "../api/MoviesDb";

function HomeScreen() {
  let [loading, setLoading] = useState(true);
  let [trending, setTrending] = useState([1, 2, 3]);
  let [movelist, setMoveList] = useState([1, 2, 3]);
  let [topRate, setTopRate] = useState([1, 2, 3]);
  let navigation = useNavigation();

  useEffect(() => {
    getTradingMovies();
    getUpcoimgMovies();
    getTopRateMovies();
  }, []);

  let getTradingMovies = async () => {
    let data = await fetchingTrandingMovies();
    if (data && data.results) {
      setTrending(data.results);
      setLoading(false);
    }
  };

  let getUpcoimgMovies = async () => {
    let data = await fetchingUpcomingMovies();
    if (data && data.results) {
      setMoveList(data.results);
      setLoading(false);
    }
  };

  let getTopRateMovies = async () => {
    let data = await fetchingTopTatingMovies();
    if (data && data.results) {
      setTopRate(data.results);
      setLoading(false);
    }
  };

  return (
    <View className="flex-1 bg-neutral-800">
      <SafeAreaView style="light">
        <View className="flex-row justify-between item-center mx-4 mt-2">
          <Octicons
            name="three-bars"
            size={25}
            color="white"
            style={style.icon}
          />
          <Text className="text-white text-3xl font-bold">
            <Text style={style.text}>M</Text>
            ovies
          </Text>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("Search");
            }}
          >
            <AntDesign
              name="search1"
              size={25}
              color="white"
              style={style.icon}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      {loading ? (
        <Loading />
      ) : (
        <ScrollView>
          {/*trending list movies */}
          {trending.length > 0 && <TreadingMovies data={trending} />}

          {/*Movies  list movies */}

          {movelist.length > 0 && (
            <MoviesList title="Upcoming" data={movelist} />
          )}

          {/*Movies  top  movies */}

          {topRate.length > 0 && <MoviesList title="Top Rate" data={topRate} />}
        </ScrollView>
      )}
    </View>
  );
}
let style = StyleSheet.create({
  icon: {
    marginTop: 5,
  },
  text: {
    color: "#eab308",
  },
  bggg: {
    backgroundColor: "#eab308",
  },
});
export default HomeScreen;
