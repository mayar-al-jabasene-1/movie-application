import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
  Platform,
  StyleSheet,
  Image,
} from "react-native";
import React, { useEffect, useState } from "react";
import { AntDesign } from "@expo/vector-icons";
import { Shadow } from "react-native-shadow-2";
import MoviesList from "../components/MoviesList";
import { useNavigation, useRoute } from "@react-navigation/native";
import Loading from "../components/Loading";
import {
  Images342,
  fallbackPersonImage,
  fetchingPersonDetails,
  fetchingPersonMovies,
} from "../api/MoviesDb";

var { width, height } = Dimensions.get("window");
const ios = Platform.OS == "ios";
const TopMargin = ios ? "" : "mt-4";

const Person = () => {
  let navigation = useNavigation();
  let [toggle, setToggle] = useState(false);
  let [dataMovies, setDataMovies] = useState([1, 2, 3, 4, 5, 6]);
  let [loading, setLoading] = useState(false);
  let [person, setPerson] = useState({});
  let { params: item } = useRoute();

  useEffect(() => {
    setLoading(true);

    getPersonDetails(item.id);
    getPersonMovies(item.id);
  }, [item]);

  let getPersonDetails = async (id) => {
    let data = await fetchingPersonDetails(id);
    if (data) setPerson(data);
    setLoading(false);
  };

  let getPersonMovies = async (id) => {
    let data = await fetchingPersonMovies(id);
    if (data && data.cast) setDataMovies(data.cast);
    setLoading(false);
  };
  return (
    <ScrollView
      className="flex-1 bg-neutral-900"
      contentContainerStyle={{ paddingBottom: 20 }}
    >
      {/*back button */}

      <SafeAreaView
        className={
          "z-20 mt-8 w-full flex-row justify-between item-center px-4 my-3" +
          TopMargin
        }
      >
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          className="rounded-xl p-1"
          style={style.bggg}
        >
          <AntDesign name="arrowleft" size={28} color="white" />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setToggle(!toggle)}>
          <AntDesign name="heart" size={28} color={toggle ? "red" : "white"} />
        </TouchableOpacity>
      </SafeAreaView>

      {/*personal details */}
      {loading ? (
        <View>
          <Loading />
        </View>
      ) : (
        <View>
          <View>
            <View
              className="flex-row justify-center"
              style={{
                shadowColor: "gray",
                shadowRadius: 40,
                shadowOffset: { width: 0, height: 5 },
                shadowOpacity: 1,
              }}
            >
              <View className="item-center rounded-full overflow-hidden w-72 h-72 border-2 border-neutral-400">
                <Image
                  // source={require("../assets/images/moviePoster2.png")}
                  source={{
                    uri: Images342(person.profile_path) || fallbackPersonImage,
                  }}
                  style={{ width: width * 0.74, height: height * 0.43 }}
                />
              </View>
            </View>
          </View>
          <View className="mt-6">
            <Text className="text-3xl text-white font-bold text-center">
              {person && person.name}
            </Text>
            <Text className="text-base text-neutral-500 text-center">
              {person && person.place_of_birth}
            </Text>

            <View className="mx-3 p-4 mt-6 flex-row justify-between items-center bg-neutral-700 rounded-full">
              <View className="border-r-2 border-r-neutral-400 px-2 items-center">
                <Text className=" text-white font-semibold">Gender</Text>
                <Text className="text-sm text-neutral-300 ">
                  {person && person.gender == 1 ? "Female" : "Male"}
                </Text>
              </View>
              <View className="border-r-2 border-r-neutral-400 px-2 items-center">
                <Text className=" text-white font-semibold">Birthday</Text>
                <Text className="text-sm text-neutral-300 ">
                  {person && person.birthday}
                </Text>
              </View>
              <View className="border-r-2 border-r-neutral-400 px-2 items-center">
                <Text className=" text-white font-semibold">Known for</Text>
                <Text className="text-sm text-neutral-300 ">
                  {person && person.known_for_department}
                </Text>
              </View>
              <View className="px-2 items-center">
                <Text className=" text-white font-semibold">Popularity</Text>
                <Text className="text-sm text-neutral-300 ">
                  {person.popularity && person.popularity.toFixed(2)}%
                </Text>
              </View>
            </View>
            <View className="my-6 mx-4">
              <Text className="text-lg text-white">Biography</Text>
              <Text className="tracking-wide text-neutral-400">
                {person ? person.biography : "N/A"}
              </Text>
            </View>
            {/*movies list */}
            <MoviesList title={"Movies"} data={dataMovies} hidden={true} />
          </View>
        </View>
      )}
    </ScrollView>
  );
};
let style = StyleSheet.create({
  icon: {
    marginTop: 5,
  },
  text: {
    color: "#eab308",
  },
  bggg: {
    backgroundColor: "#eab308",
  },
});

export default Person;
