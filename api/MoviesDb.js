import axios from "axios";
import { Path } from "react-native-svg";

let apiKey = "09252643ef55239f9641f4a36d6a8adf";

//endpoint

const apiBaseUrl = "https://api.themoviedb.org/3";
const trandingMovies = `${apiBaseUrl}/trending/movie/day?api_key=${apiKey}`;
const UpcomingMovies = `${apiBaseUrl}/movie/upcoming?api_key=${apiKey}`;
const TopRatingMovies = `${apiBaseUrl}/movie/top_rated?api_key=${apiKey}`;
const SearchMoviesEndPoint = `${apiBaseUrl}/search/movie?api_key=${apiKey}`;

//dynamic endpoint
const MovieDetailsEndPoint = (id) => {
  return `${apiBaseUrl}/movie/${id}?api_key=${apiKey}`;
};
const MovieCreditEndPoint = (id) => {
  return `${apiBaseUrl}/movie/${id}/credits?api_key=${apiKey}`;
};
const SimilerMoviesEndPoint = (id) => {
  return `${apiBaseUrl}/movie/${id}/similar?api_key=${apiKey}`;
};

const PersonalDetailsEndPoint = (id) => {
  return `${apiBaseUrl}/person/${id}?api_key=${apiKey}`;
};

const PersonalMoviesEndPoint = (id) => {
  return `${apiBaseUrl}/person/${id}/movie_credits?api_key=${apiKey}`;
};

// fallback images
export const fallbackMoviePoster =
  "https://img.myloview.com/stickers/white-laptop-screen-with-hd-video-technology-icon-isolated-on-grey-background-abstract-circle-random-dots-vector-illustration-400-176057922.jpg";
export const fallbackPersonImage =
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmUiF-YGjavA63_Au8jQj7zxnFxS_Ay9xc6pxleMqCxH92SzeNSjBTwZ0l61E4B3KTS7o&usqp=CAU";

//images
export const Images600 = (path) => {
  return path ? `https://image.tmdb.org/t/p/w500${path}` : null;
};
export const Images342 = (path) => {
  return path ? `https://image.tmdb.org/t/p/w342${path}` : null;
};
export const Images185 = (path) => {
  return path ? `https://image.tmdb.org/t/p/w185${path}` : null;
};

//fucntion call api
let apicall = async (endpoint, params) => {
  const option = {
    method: "Get",
    url: endpoint,
    params: params ? params : {},
  };
  try {
    let response = await axios.request(option);
    return response.data;
  } catch (error) {}
};

export const fetchingTrandingMovies = () => {
  return apicall(trandingMovies);
};

export const fetchingUpcomingMovies = () => {
  return apicall(UpcomingMovies);
};

export const fetchingTopTatingMovies = () => {
  return apicall(TopRatingMovies);
};

export const fetchingMoviesDetails = (id) => {
  return apicall(MovieDetailsEndPoint(id));
};
export const fetchingCreditDetails = (id) => {
  return apicall(MovieCreditEndPoint(id));
};
export const fetchingSimilerMovies = (id) => {
  return apicall(SimilerMoviesEndPoint(id));
};
export const fetchingPersonDetails = (id) => {
  return apicall(PersonalDetailsEndPoint(id));
};
export const fetchingPersonMovies = (id) => {
  return apicall(PersonalMoviesEndPoint(id));
};

export const fetchingSearchMovies = (params) => {
  return apicall(SearchMoviesEndPoint, params);
};
