import { View, Text, TouchableOpacity, Image, Dimensions } from "react-native";
import React from "react";
import { ScrollView } from "react-native";
import { Images185, fallbackPersonImage } from "../api/MoviesDb";

const Cost = ({ cost, navegation }) => {
  var { width, height } = Dimensions.get("window");
  castName = "Kean Reavers";
  personName = "jone weak";
  return (
    <View className="my-6">
      <Text className="text-white text-2xl mx-4 mb-5">Top Cast</Text>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={true}
        contentContainerStyle={{ paddingHorizontal: 15 }}
      >
        {cost &&
          cost.map((e, index) => {
            return (
              <TouchableOpacity
                onPress={() => navegation.navigate("Person", e)}
                className="mr-4 items-center"
                key={index}
              >
                <View className="overflow-hidden rounded-full h-20 w-20 item-center border border-neutral-400">
                  <Image
                    // source={require("../assets/images/castImage1.png")}
                    source={{
                      uri: Images185(e.profile_path) || fallbackPersonImage,
                    }}
                    className="rounded-2xl h-24 w-20"
                  />
                </View>
                <Text className="text-white text-xs mt-1">
                  {e.character && e.character.length > 10
                    ? e.character.slice(0, 10)
                    : e.character}
                </Text>
                <Text className="text-neutral-400 text-xs mt-1">
                  {e.original_name && e.original_name.length > 10
                    ? e.original_name.slice(0, 10)
                    : e.original_name}
                </Text>
              </TouchableOpacity>
            );
          })}
      </ScrollView>
    </View>
  );
};

export default Cost;
