import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Dimensions,
} from "react-native";
import React from "react";
import { FlatList } from "react-native";
import { Image } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Images185, Images342 } from "../api/MoviesDb";

var { width, height } = Dimensions.get("window");

const MoviesList = ({ title, data, hidden }) => {
  let navigation = useNavigation();
  let movieName = "spider man And BatMan And Hulk";
  return (
    <View className="mb-8 mt-3">
      <View className="flex-row justify-between item-center mx-4 mt-2 mb-3">
        <Text className="text-white text-xl">{title}</Text>
        {!hidden && (
          <TouchableOpacity>
            <Text style={style.text} className="text-lg">
              See All
            </Text>
          </TouchableOpacity>
        )}
      </View>

      {/*move row */}
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ paddingHorizontal: 15 }}
      >
        {data.map((item, index) => {
          return (
            <TouchableOpacity
              onPress={() => navigation.push("Movie", item)}
              key={index}
            >
              <View className="space-y-l mr-4">
                <Image
                  source={{ uri: Images185(item.poster_path) }}
                  className="rounded-3xl mb-1"
                  style={{ width: width * 0.33, height: height * 0.22 }}
                />
                <Text className="text-white">
                  {item.title && item.title.length > 14
                    ? item.title.slice(0, 14) + "..."
                    : item.title}
                </Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

let style = StyleSheet.create({
  icon: {
    marginTop: 5,
  },
  text: {
    color: "#eab308",
  },
  bggg: {
    backgroundColor: "#eab308",
  },
});
export default MoviesList;
