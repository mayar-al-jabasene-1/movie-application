import { useNavigation } from "@react-navigation/native";
import React from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import Carousel from "react-native-snap-carousel";
import { Images600, fallbackMoviePoster } from "../api/MoviesDb";

var { width, height } = Dimensions.get("window");
function TreadingMovies({ data }) {
  let navigation = useNavigation();

  let handleclick = (item) => {
    navigation.navigate("Movie", item);
  };

  return (
    <View className="mb-8 mt-3">
      <Text className="text-white mx-4 mb-6 text-xl ">Trending</Text>
      <Carousel
        data={data}
        firstItem={1}
        renderItem={({ item }) => (
          <Teading item={item} handleclick={handleclick} />
        )}
        sliderWidth={width}
        inactiveSlideOpacity={0.6}
        itemWidth={width * 0.62}
        slideStyle={{ display: "flex", alignItems: "center" }}
      />
    </View>
  );
}

function Teading({ item, handleclick }) {
  return (
    <TouchableWithoutFeedback onPress={() => handleclick(item)}>
      <View>
        <Image
          source={{ uri: Images600(item.poster_path) || fallbackMoviePoster }}
          style={{ width: width * 0.6, height: height * 0.4 }}
          className="rounded-3xl"
        />
      </View>
    </TouchableWithoutFeedback>
  );
}
export default TreadingMovies;
