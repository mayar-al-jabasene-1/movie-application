import { View, Text, Dimensions } from "react-native";
import React from "react";
var { width, height } = Dimensions.get("window");
import * as Progress from "react-native-progress";

const Loading = () => {
  return (
    <View
      style={{ width, height }}
      className="flex-row  justify-center items-center"
    >
      <Progress.CircleSnail thickness={12} size={160} color={"#eab308"} />
    </View>
  );
};

export default Loading;
